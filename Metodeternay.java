import java.util.Scanner;

public class Metodeternay {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jumlah SKS yang diambil:");
        int sks = input.nextInt();

        int biaya = 75000 * sks;
        int denda = (sks > 24) ? (sks - 24) * 5000 : 0;

        System.out.println("Total biaya yang harus dibayar: Rp " + (biaya + denda));
    }
}
