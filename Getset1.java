
    import java.util.Scanner;

class KalkulatorGetSet {
    private int angka1;
    private int angka2;

    public int getAngka1() {
        return angka1;
    }

    public void setAngka1(int angka1) {
        this.angka1 = angka1;
    }

    public int getAngka2() {
        return angka2;
    }

    public void setAngka2(int angka2) {
        this.angka2 = angka2;
    }
public int tambah() {
        return angka1 + angka2;
    }

    public int kurang() {
        return angka1 - angka2;
    }
}

public class Getset1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        KalkulatorGetSet kalkulator = new KalkulatorGetSet();

        System.out.print("Masukkan angka pertama: ");
        int angka1 = input.nextInt();
        kalkulator.setAngka1(angka1);

        System.out.print("Masukkan angka kedua: ");
        int angka2 = input.nextInt();
        kalkulator.setAngka2(angka2);

        System.out.println("Hasil penjumlahan: " + kalkulator.tambah());
        System.out.println("Hasil pengurangan: " + kalkulator.kurang());
}
}