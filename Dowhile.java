import java.util.Scanner;

public class Dowhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int sks;

        do {
            System.out.println("Masukkan jumlah SKS yang diambil:");
            sks = input.nextInt();
            if (sks <= 0)
                System.out.println("Masukkan jumlah SKS yang valid.");
        } while (sks <= 0);

        int biaya = 75000 * sks;

        if (sks <= 24) {
            System.out.println("Total biaya yang harus dibayar: Rp " + biaya);
        } else {
            int denda = (sks - 24) * 5000;
            System.out.println("Anda terlambat membayar dengan " + (sks - 24) + " SKS lebih.");
            System.out.println("Total biaya yang harus dibayar: Rp " + (biaya + denda));
        }
    }
}

